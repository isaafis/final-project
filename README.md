### Final Project
### Installation

1. Clone the repository using the command "git clone [link]"
2. Create database in MySql
3. Configure the .env file accordingly
4. Run command

```
$composer install
$php artisan migrate
$php artisan db:seed
$php artisan serve
$php artisan storage:link
```

### Built With

-   Bootstrap- CSS framework
-   JQuery- Javascript framework
-   Laravel - PHP framework
-   MySql- Databse

### Kelompok 1

### Nama Kelompok

-   Isa Afisyah Jamil
-   Eko
-   Ramdhani

### Tema

Toko Sepatu

### ERD
![final](/uploads/fb9d15999ad8986a572681bea53fe698/final.png)

### link video

Link demo aplikasi : https://youtu.be/omENYend8rM
link deploy (heroku) : http://final-project-kelompok-1.herokuapp.com/

### Template

Colorlib.
